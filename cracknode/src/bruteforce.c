#include "bruteforce.h"
#include "worker.h"
#include "serverresponse.h"
#include "datastreamsender.h"
#include "commoncrypt.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <pthread.h>
#include <unistd.h>

#define BUFFER_SIZE 2048

struct _bruteforceWrapper{
    Worker *worker;
    char* alphabet;
    char* begword;
    unsigned int maxwordsize;
    unsigned int chunksize;
};

void freeBruteForceWrapper(struct _bruteforceWrapper *bfw){
    if(bfw){
        if(bfw->alphabet){ free(bfw->alphabet); bfw->alphabet=NULL;}
        if(bfw->begword){ free(bfw->begword); bfw->begword=NULL;}
    }
}

int findPosInAlphabet(char c,const char* alphabet){
    int i;
    for(i=0;alphabet[i];++i) if(alphabet[i]==c) return i;
    return -1;
}

void bruteforceCleaner(void *data){
    if(data && *((char**)data)) free(*((char**)data));
}

int bruteforce(Worker *worker,const char* alphabet, const char* begword, unsigned int maxWordSize, unsigned int iterations){
    unsigned int size=strlen(begword);
    unsigned int i,j;
    int match=0;
    int alphsize=strlen(alphabet);
    char* wordcont=NULL;
    int* wordindices=NULL;
    pthread_cleanup_push(bruteforceCleaner,&wordcont);
    wordcont=(char*)malloc(maxWordSize+1);
    pthread_cleanup_push(bruteforceCleaner,&wordindices);
    wordindices=(int*)malloc(sizeof(int)*maxWordSize);
    strncpy(wordcont,begword,size);
    wordcont[size]=0;
    for(i=0;i<size;++i){ wordindices[i]=findPosInAlphabet(begword[i],alphabet); }
    for(i=0,j=size-1;i<iterations && !(match=checkPasswordWithHash(wordcont,worker->cryptHash));++i){
        workerLock(worker);
        if(worker->workerState==WORKER_CANCEL){
            workerUnlock(worker);
            break;
        }
        workerUnlock(worker);
        for(;j>0 && wordindices[j]==(alphsize-1);--j){
            wordindices[j]=0;
            wordcont[j]=alphabet[0];
        }
        if(!j && wordindices[j]==(alphsize-1)){
            wordindices[j]=0;
            wordcont[j]=alphabet[0];
            if(++size<=maxWordSize){
                wordindices[size-1]=0;
                wordcont[size-1]=alphabet[0];
                wordcont[size]=0;
            }
            else break;
        }
        else wordcont[j]=alphabet[++wordindices[j]];
        j=size-1;
    }
    if(match){
        workerLock(worker);
        worker->password=wordcont;
        workerUnlock(worker);
    }
    else free(wordcont);
    pthread_cleanup_pop(1);
    pthread_cleanup_pop(0);
    return match;
}

void bfwCleanUp(void *data){
    if(data){
        workerLock(((struct _bruteforceWrapper*)data)->worker);
        freeWorker(((struct _bruteforceWrapper*)data)->worker);
        workerUnlock(((struct _bruteforceWrapper*)data)->worker);
        freeBruteForceWrapper((struct _bruteforceWrapper*)data);
        free(data);
    }
}

void* bruteforceThread(void* data){
    struct _bruteforceWrapper *bfw=(struct _bruteforceWrapper*)data;
    pthread_cleanup_push(bfwCleanUp,bfw);
    bruteforce(bfw->worker,bfw->alphabet,bfw->begword,bfw->maxwordsize,bfw->chunksize);
    
    responseStreamSender(bfw->worker,SERVER_RESPONSE_DONE);
    
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}

int bruteforceStreamHandler(Worker* worker){
    ssize_t off;
    char buffer[BUFFER_SIZE];
    struct _bruteforceWrapper * bfw=(struct _bruteforceWrapper*)malloc(sizeof(struct _bruteforceWrapper));
    pthread_cleanup_push(bfwCleanUp,bfw);
    bfw->worker=worker;
    bfw->alphabet=NULL;
    bfw->begword=NULL;
    if((off=parseNextStream(worker->srcStream.fd,buffer,BUFFER_SIZE))<=0){
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"Expected alphabet but got nothing");
        workerUnlock(worker);
        free(bfw);
        return 0;
    }
    bfw->alphabet=strncpy((char*)malloc(off+1),buffer,off);
    (bfw->alphabet)[off]=0;
    
    if((off=parseNextStream(worker->srcStream.fd,buffer,BUFFER_SIZE))<=0){
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"Expected Maximum word size but got nothing");
        workerUnlock(worker);
        freeBruteForceWrapper(bfw);
        free(bfw);
        return 0;
    }
    buffer[off]=0;
    char* endptr=NULL;
    errno=0;
    long int maxwordsize=strtol(buffer,&endptr,10);
    if(errno || (endptr && *endptr) || maxwordsize<1){
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"Wrong value for maximum word size");
        workerUnlock(worker);
        freeBruteForceWrapper(bfw);
        free(bfw);
        return 0;
    }
    bfw->maxwordsize=maxwordsize;
    
    if((off=parseNextStream(worker->srcStream.fd,buffer,BUFFER_SIZE))<=0){
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"Expected starting word but got nothing");
        workerUnlock(worker);
        freeBruteForceWrapper(bfw);
        free(bfw);
        return 0;
    }
    bfw->begword=strncpy((char*)malloc(off+1),buffer,off);
    (bfw->begword)[off]=0;
    
    if((off=parseNextStream(worker->srcStream.fd,buffer,BUFFER_SIZE))<=0){
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"Expected chunksize but got nothing");
        workerUnlock(worker);
        freeBruteForceWrapper(bfw);
        free(bfw);
        return 0;
    }
    buffer[off]=0;
    errno=0;
    long int chunksize=strtol(buffer,&endptr,10);
    if(errno || (endptr && *endptr) || maxwordsize<1){
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"Wrong value for chunksize");
        workerUnlock(worker);
        freeBruteForceWrapper(bfw);
        free(bfw);
        return 0;
    }
    bfw->chunksize=chunksize;
    worker->cont=1;
    
    if(workerLock(worker)){
        freeBruteForceWrapper(bfw);
        free(bfw);
        return 0;
    }
    if(pthread_create(&(worker->thread),NULL,bruteforceThread,bfw)){
        workerUnlock(worker);
        perror("bruteforceStreamHandler: pthread_create");
        freeBruteForceWrapper(bfw);
        free(bfw);
        return 0;
    }
    workerUnlock(worker);
    pthread_cleanup_pop(0);
    return 1;
}
