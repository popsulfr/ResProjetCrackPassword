#ifndef _BRUTEFORCE_H
#define _BRUTEFORCE_H

#include "worker.h"

int bruteforce(Worker* worker,const char* alphabet,const char* begword,unsigned int maxWordSize,unsigned int iterations);

int bruteforceStreamHandler(Worker* worker);
#endif
