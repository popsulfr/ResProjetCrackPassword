#include "dict.h"
#include "commoncrypt.h"
#include "worker.h"
#include "serverresponse.h"

#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define BUFFER_SIZE 2048
#define DELIMITER "\n"

int dictCheck(Worker* worker){
    workerLock(worker);
    int fd=worker->srcStream.fd;
    workerUnlock(worker);
    
    char buffer[BUFFER_SIZE];
    ssize_t size;
    
    size_t off=0;
    for(;(size=read(fd,buffer+off,BUFFER_SIZE-1-off))>0 || (size==0 && off>0);){
        workerLock(worker);
        if(worker->workerState==WORKER_CANCEL){
            workerUnlock(worker);
            break;
        }
        workerUnlock(worker);
        buffer[size+off]=0;
        off=0;
        char* saveptr=NULL;
        char* s;
        for(s=strtok_r(buffer,DELIMITER,&saveptr);s;s=strtok_r(NULL,DELIMITER,&saveptr)){
            if((buffer!=s) && (!saveptr || ((saveptr)-buffer)>=(BUFFER_SIZE-1))){
                memmove(buffer,s,(off=(BUFFER_SIZE-1)-(s-buffer)));
                break;
            }
            else{
                if(checkPasswordWithHash(s,worker->cryptHash)){
                    size_t len=strlen(s);
                    workerLock(worker);
                    worker->password=strncpy((char*)malloc(len+1),s,len);
                    (worker->password)[len]=0;
                    workerUnlock(worker);
                    return 1;
                }
            }
        }
    }
    return 0;
}

void workerCleanUp(void *data){
    if(data){
        workerLock((Worker*)data);
        freeWorker((Worker*)data);
        workerUnlock((Worker*)data);
    }
}

void* dictThread(void* data){
    Worker *worker=(Worker*)data;
    pthread_cleanup_push(workerCleanUp,worker);
    dictCheck(worker);
    worker->cont=1;
    
    responseStreamSender(worker,SERVER_RESPONSE_DONE);
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}


int dictStreamHandler(Worker* worker){
    workerLock(worker);
    if(pthread_create(&(worker->thread),NULL,dictThread,worker)){
        workerUnlock(worker);
        perror("dictStreamHandler: pthread_create");
        return 0;
    }
    workerUnlock(worker);
    return 1;
}
