#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <errno.h>
#include <arpa/inet.h>
#include <errno.h>
#include <limits.h>

#include "connectionlistener.h"
#include "clientrequest.h"

#define BUFFER_SIZE 2048

Listener _tcpList;
Listener *TCPList=NULL;

Listener _udpList;
Listener *UDPList=NULL;

struct _dataUDPWrapper{
    int sock;
    void (*packetHandler)(DataPacket*);
    DataPacket dp;
};

struct _dataTCPWrapper{
    int sock;
    void (*streamHandler)(DataStream*);
    DataStream ds;
};

void initDataPacket(DataPacket *dp){
    dp->thread=0;
    dp->length=0;
    dp->data=(char*)malloc(sizeof(char)*BUFFER_SIZE);
    dp->listenPort=htons(0);
    memset(&(dp->src_address),0,sizeof(struct sockaddr_in));
}

void initDataStream(DataStream *ds){
    ds->fd=-1;
    ds->thread=0;
    ds->listenPort=htons(0);
    memset(&(ds->src_address),0,sizeof(struct sockaddr_in));
}

void freeDataPacket(DataPacket *dp){
    if(dp && dp->data){ free(dp->data); dp->data=NULL;}
}

void freedataUDPWrapper(struct _dataUDPWrapper* duw){
    if(duw){
        freeDataPacket(&(duw->dp));
    }
}

ssize_t parseNextPacket(const char* buffer,size_t offset,size_t size){
    if(!buffer || !size || offset>=size) return -1;
    size_t i;
    for(i=offset;i<size && buffer[i]!=CLIENT_SEPARATOR;++i);
    return i;
}

ssize_t parseNextStream(int fd,char *buffer,size_t size){
    if(!buffer || !size) return -1;
    int t;
    char c;
    size_t i;
    for(i=0;i<size && ((t=read(fd,&c,1))>0) && c!=CLIENT_SEPARATOR;buffer[i++]=c);
    return i;
}

ssize_t skipStream(int fd,size_t size){
    return lseek(fd,size,SEEK_CUR);
}

void closeConnection(int* fd){
    if(fd){
        shutdown(*fd,SHUT_RDWR);
        close(*fd);
        *fd=-1;
    }
}

/*
 * Tests if the header is contained
 * Returns the new size
 */
int testUDPHeader(DataPacket *dp){
    ssize_t off;
    if(((off=parseNextPacket(dp->data,0,dp->length))>0) && !strncmp((char*)(dp->data),CLIENT_HEADER,off)){
        ++off;
        if(off<(ssize_t)(dp->length)){
            memmove(dp->data,&(dp->data[off]),dp->length-off);
            dp->length=dp->length-off;
        }
        else{
            free(dp->data);
            dp->data=NULL;
            dp->length=0;
        }
        return 1;
    }
    return 0;
}

int testUDPPort(DataPacket *dp){
    ssize_t off;
    if((off=parseNextPacket(dp->data,0,dp->length))>0 && off<16){
        char buffer[16];
        memset(buffer,0,16);
        memcpy(buffer,dp->data,off);
        char *endptr=NULL;
        errno=0;
        long int tmp=strtol(buffer,&endptr,10);
        if(!errno && (!endptr || !(*endptr)) && tmp>0 && tmp<=USHRT_MAX){
            dp->listenPort=htons((uint16_t)tmp);
        }
        ++off;
        if(off<(ssize_t)(dp->length)){
            memmove(dp->data,&(dp->data[off]),dp->length-off);
            dp->length=dp->length-off;
        }
        else{
            free(dp->data);
            dp->data=NULL;
            dp->length=0;
        }
        return 1;
    }
    return 0;
}

int testTCPPort(DataStream *ds){
    ssize_t len;
    char buffer[16];
    if((len=parseNextStream(ds->fd,buffer,16)) && len<16){
        buffer[len]=0;
        char *endptr=NULL;
        errno=0;
        long int tmp=strtol(buffer,&endptr,10);
        if(!errno && (!endptr || !(*endptr)) && tmp>0 && tmp<USHRT_MAX){
            ds->listenPort=htons((uint16_t)tmp);
        }
        return 1;
    }
    return 0;
}

int testTCPHeader(DataStream *ds){
    ssize_t size=strlen(CLIENT_HEADER);
    char* tmp=(char*)malloc(size+2);
    ssize_t len;
    int b=(((len=parseNextStream(ds->fd,tmp,size+2))==size) && (!strncmp(CLIENT_HEADER,tmp,size)));
    free(tmp);
    return b;
}

void destroyListener(Listener *list){
    if(!list) return;
    if(list->sock>-1){
        shutdown(list->sock, SHUT_RDWR);
        close(list->sock);
        list->sock=-1;
    }
    if(list->thread>0){
        pthread_join(list->thread,NULL);
        list->thread=0;
    }
}

/*
 * Prints 'Incoming [ip:port] '
 */
void connectionPrinter(FILE *file,struct in_addr addr,in_port_t port,const char* prefix,const char* addInfo){
    ConnectionIdBuffer cid;
    if(file && (connectionIdCreator(&cid,addr,port))){
        fprintf(file,"%s%s%s%s%s\n",(prefix && *prefix)?prefix:"",
                (prefix && *prefix)?" ":"",
                cid.buffer,
                (addInfo && *addInfo)?" ":"",
                (addInfo && *addInfo)?addInfo:"");
    }
}

char* connectionIdCreator(ConnectionIdBuffer* cid,struct in_addr addr,in_port_t port){
    if(cid){
        char buffer[INET_ADDRSTRLEN];
        if(!inet_ntop(AF_INET,&addr,buffer,INET_ADDRSTRLEN)){
            perror("connectionIdCreator: inet_ntop");
            return NULL;
        }
        snprintf(cid->buffer,CONNECTION_ID_BUFFER_SIZE,"[%.*s:%d]",INET_ADDRSTRLEN,buffer,htons(port));
        return cid->buffer;
    }
    return NULL;
}

int compareAdresses(const DataStream *ds1,const DataStream *ds2){
    if(ds1 && ds2){
        return (memcmp(&(ds1->src_address.sin_addr),&(ds2->src_address.sin_addr),sizeof(struct in_addr))
        || memcmp(&(ds1->listenPort),&(ds2->listenPort),sizeof(in_port_t)));
    }
    return 1;
}

void udpConnectionHandlerCleaner(void* data){
    if(data){
        freedataUDPWrapper(((struct _dataUDPWrapper*)data));
        free(data);
    }
}

void* handleUDPConnection(void* data){
    struct _dataUDPWrapper* duw=(struct _dataUDPWrapper*)data;
    pthread_cleanup_push(udpConnectionHandlerCleaner,duw);
    if(testUDPHeader(&(duw->dp))){
        testUDPPort(&(duw->dp));
        if(duw->packetHandler) duw->packetHandler(&(duw->dp));
    }
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}

void tcpConnectionHandlerCleaner(void* data){
    if(data){
        free(data);
    }
}

void * handleTCPConnection(void* data){
    struct _dataTCPWrapper *dtw=((struct _dataTCPWrapper*)data);
    pthread_cleanup_push(tcpConnectionHandlerCleaner,dtw);
    if(testTCPHeader(&(dtw->ds))){
        testTCPPort(&(dtw->ds));
        if(dtw->streamHandler) dtw->streamHandler(&(dtw->ds));
    }
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}

void listenerLoopCleanerAttr(void* data){
    if(data){
        pthread_attr_destroy((pthread_attr_t*)data);
    }
}

void udpListenerLoopCleanerDUW(void* data){
    if(data){
        if(*((struct _dataUDPWrapper**)data)){
            freedataUDPWrapper(*((struct _dataUDPWrapper**)data));
            free(*((struct _dataUDPWrapper**)data));
        }
    }
}

void sockaddrFiller(struct sockaddr_in* address,uint32_t hostaddr,uint16_t port){
    memset(address,0,sizeof(struct sockaddr_in));
    address->sin_family=AF_INET;
    address->sin_addr.s_addr=htonl(hostaddr);
    address->sin_port=htons(port);
}

int socketCreator(int type, int protocol){
    int sock;
    if((sock=socket(AF_INET,type,protocol))<0){
        perror("socketCreator: socket");
        return sock;
    }
    if(type==SOCK_STREAM){
        int sockoptval=1;
        setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &sockoptval, sizeof(int));
    }
    return sock;
}


int socketBinder(int sock,struct sockaddr_in* address){
    int ret;
    if((ret=bind(sock,(struct sockaddr*)address,sizeof(struct sockaddr_in)))<0){
        perror("socketBinder: bind");
        shutdown(sock,SHUT_RDWR);
        close(sock);
    }
    return ret;
}

/*
 * Infinite UDP loop taking a struct _dataUDPWrapper
 * Incoming connections are handled in a detached thread
 */
void* udpListenerLoop(void *data){
    struct _dataUDPWrapper *duw=(struct _dataUDPWrapper*)data;
    fprintf(stderr,"UDP: Listening to incoming messages...\n");
    
    struct sockaddr_in remote_address;
    socklen_t remote_address_len=sizeof(remote_address);
    ssize_t recvlen;
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    pthread_cleanup_push(listenerLoopCleanerAttr,&attr);
    struct _dataUDPWrapper* pduw=NULL;
    pthread_cleanup_push(udpListenerLoopCleanerDUW,&pduw);
    pduw=duw;
    initDataPacket(&((pduw)->dp));
    for(;;){
        if((recvlen=recvfrom((pduw)->sock,(pduw)->dp.data,BUFFER_SIZE,0,(struct sockaddr*)(&remote_address),&remote_address_len))<0){
            perror("udpListenerLoop: recvfrom");
            pthread_exit(NULL);
        }
        if(recvlen>0){
            (pduw)->dp.length=recvlen;
            (pduw)->dp.src_address=remote_address;
            (pduw)->dp.listenPort=remote_address.sin_port;
            struct _dataUDPWrapper* newpduw=(struct _dataUDPWrapper*)malloc(sizeof(struct _dataUDPWrapper));
            memcpy(newpduw,(pduw),sizeof(struct _dataUDPWrapper));
            if(pthread_create(&((pduw)->dp.thread),&attr,handleUDPConnection,pduw)){
                perror("udpListenerLoop: pthread_create");
                free(newpduw);
                pthread_exit(NULL);
            }
            pduw=newpduw;
            initDataPacket(&((pduw)->dp));
        }
    }
    pthread_cleanup_pop(1);
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}

/*
 * Creates UDP server on port, the Listener structure is filled with socket and thread
 * packetHandler handles the incoming packets
 * The recvfrom loop is in its own thread
 */
int listenConnectionsUDP(uint16_t port,Listener *list,void (*packetHandler)(DataPacket*)){
    if(!list) return 0;
    fprintf(stderr,"UDP: Creating Socket with port %d\n",port);
    if((list->sock=socketCreator(SOCK_DGRAM,0))<0) return 0;
    
    struct sockaddr_in host_address;
    sockaddrFiller(&host_address,INADDR_ANY,port);
    
    fprintf(stderr,"UDP: Binding...\n");
    if(socketBinder(list->sock,&host_address)<0) return 0;
    
    struct _dataUDPWrapper *duw=(struct _dataUDPWrapper*)malloc(sizeof(struct _dataUDPWrapper));
    duw->sock=list->sock;
    duw->packetHandler=packetHandler;
    
    if(pthread_create(&(list->thread),NULL,udpListenerLoop,duw)){
        perror("listenConnectionsUDP: pthread_create");
        freedataUDPWrapper(duw);
        free(duw);
        closeConnection(&(list->sock));
        return 0;
    }
    return 1;
}

/*
 * struct _dataTCPWrapper thread cleaner for pthread_cleanup_push
 */
void tcpListenerLoopCleanerDTW(void* data){
    if(data){
        if(*((struct _dataTCPWrapper**)data)){
            free(*((struct _dataTCPWrapper**)data));
        }
    }
}

/*
 * Infinite TCP accept loop taking a struct _dataTCPWrapper
 * Incoming connections are handled in a detached thread
 */
void* tcpListenerLoop(void *data){
    struct _dataTCPWrapper *dtw=(struct _dataTCPWrapper*)data;
    fprintf(stderr,"TCP: Listening to incoming messages...\n");
    
    struct sockaddr_in remote_address;
    socklen_t remote_address_len=sizeof(remote_address);
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    pthread_cleanup_push(listenerLoopCleanerAttr,&attr);
    struct _dataTCPWrapper* pdtw=NULL;
    pthread_cleanup_push(tcpListenerLoopCleanerDTW,&pdtw);
    pdtw=dtw;
    initDataStream(&((pdtw)->ds));
    for(;;){
        int fd;
        if((fd=accept((pdtw)->sock,(struct sockaddr*)(&remote_address),&remote_address_len))<0){
            perror("tcpListenerLoop: accept");
            pthread_exit(NULL);
        }
        (pdtw)->ds.fd=fd;
        (pdtw)->ds.src_address=remote_address;
        (pdtw)->ds.listenPort=remote_address.sin_port;
        struct _dataTCPWrapper *newpdtw=(struct _dataTCPWrapper*)malloc(sizeof(struct _dataTCPWrapper));
        memcpy(newpdtw,(pdtw),sizeof(struct _dataTCPWrapper));
        if(pthread_create(&((pdtw)->ds.thread),&attr,handleTCPConnection,pdtw)){
            perror("tcpListenerLoop: pthread_create");
            free(newpdtw);
            pthread_exit(NULL);
        }
        pdtw=newpdtw;
        initDataStream(&((pdtw)->ds));
    }
    pthread_cleanup_pop(1);
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}

/*
 * Creates a TCP Server on port, the Listener structure is filled with the socket and thread
 * A streamHandler is used to handle the incoming stream
 * The listener loop is in its own thread
 */
int listenConnectionsTCP(uint16_t port,Listener *list,void (*streamHandler)(DataStream *)){
    if(!list) return 0;
    fprintf(stderr,"TCP: Creating TCP Socket with port %d\n",port);
    if((list->sock=socketCreator(SOCK_STREAM,0))<0) return 0;
    
    struct sockaddr_in host_address;
    sockaddrFiller(&host_address,INADDR_ANY,port);
    
    fprintf(stderr,"TCP: Binding...\n");
    if(socketBinder(list->sock,&host_address)<0) return 0;
    
    if(listen(list->sock,MAX_CONNECTIONS)<0){
        perror("listenConnectionsTCP: listen");
        shutdown(list->sock,SHUT_RDWR);
        close(list->sock);
        return 0;
    }
    
    struct _dataTCPWrapper *dtw=(struct _dataTCPWrapper*)malloc(sizeof(struct _dataTCPWrapper));
    dtw->sock=list->sock;
    dtw->streamHandler=streamHandler;
    
    if(pthread_create(&(list->thread),NULL,tcpListenerLoop,dtw)){
        perror("listenConnectionsTCP: pthread_create");
        free(dtw);
        closeConnection(&(list->sock));
        return 0;
    }
    return 1;
}
