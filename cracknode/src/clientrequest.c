#include "clientrequest.h"
#include "connectionlistener.h"
#include "serverresponse.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>

const char* ClientRequestStrings[]={
    "DISCOVER",
    "PREPARE",
    "START",
    "STOP",
    NULL
};

ServerResponse clientRequest_PREPARE(DataStream* ds_src,Worker* worker);
ServerResponse clientRequest_START(DataStream* ds_src,Worker* worker);
ServerResponse clientRequest_STOP(DataStream* ds_src,Worker* worker);

struct clientRequestFunc{ServerResponse (*clientRequestFunc)(DataStream*,Worker*);};

struct clientRequestFunc ClientRequestFuncs[]={
    {NULL},
    {clientRequest_PREPARE},
    {clientRequest_START},
    {clientRequest_STOP},
    {NULL}
};

ClientRequest getClientRequestFromString(const char* string){
    ClientRequest cr=CLIENT_REQUEST_UNKNOWN;
    if(string)
        for(cr=0;cr<CLIENT_REQUEST_UNKNOWN && ClientRequestStrings[cr] && strcmp(string,ClientRequestStrings[cr]);++cr);
    return cr;
}

ClientRequest getClientRequestFromString_size(const char* string,size_t size){
    ClientRequest cr=CLIENT_REQUEST_UNKNOWN;
    if(string)
        for(cr=0;cr<CLIENT_REQUEST_UNKNOWN && ClientRequestStrings[cr] && strncmp(string,ClientRequestStrings[cr],size);++cr);
        return cr;
}

const char* getClientRequestStringFromVal(ClientRequest cr){
    return (cr<=CLIENT_REQUEST_UNKNOWN)?ClientRequestStrings[cr]:NULL;
}

char* unknownRequestStringCreator(const char* string,size_t size);

ServerResponse handleClientRequestDataPacket(const DataPacket* dp_src,Worker* worker){
    (void)worker;
    if(!dp_src) return SERVER_RESPONSE_UNKNOWN;
    ssize_t off;
    if((off=parseNextPacket(dp_src->data,0,dp_src->length))>0 &&
        !strncmp((char*)(dp_src->data),getClientRequestStringFromVal(CLIENT_REQUEST_DISCOVER),off)){
        connectionPrinter(stdout,dp_src->src_address.sin_addr,dp_src->src_address.sin_port,UDP_INCOMING_PREFIX,getClientRequestStringFromVal(CLIENT_REQUEST_DISCOVER));
        return SERVER_RESPONSE_SIGNAL;
    }
    else{
        if(off>0){
            char* tmp=unknownRequestStringCreator(dp_src->data,off);
            connectionPrinter(stderr,dp_src->src_address.sin_addr,dp_src->src_address.sin_port,UDP_INCOMING_PREFIX,tmp);
            free(tmp);
        }
        else{
            connectionPrinter(stderr,dp_src->src_address.sin_addr,dp_src->src_address.sin_port,UDP_INCOMING_PREFIX,"Unkown Request");
        }
    }
    return SERVER_RESPONSE_UNKNOWN;
}

void* prepareTimeoutThread(void* data){
    Worker* worker=(Worker*)data;
    sleep(CLIENT_PREPARE_TIMEOUT);
    workerLock(worker);
    if(worker->workerState==WORKER_RESERVED){
        worker->workerState=WORKER_WAITING;
    }
    workerUnlock(worker);
    pthread_exit(NULL);
    return NULL;
}

ServerResponse clientRequest_PREPARE(DataStream* ds_src,Worker* worker){
    worker->cont=1;
    ServerResponse ret=SERVER_RESPONSE_UNKNOWN;
    if(workerLock(worker)) return 0;
    if(worker->workerState==WORKER_WAITING){
        worker->workerState=WORKER_RESERVED;
        memcpy(&(worker->srcStream),ds_src,sizeof(DataStream));
        workerUnlock(worker);
        
        pthread_t thread;
        pthread_attr_t attr;
        pthread_attr_init(&attr);
        pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
        if(pthread_create(&thread,&attr,prepareTimeoutThread,worker)){
            perror("clientRequest_PREPARE: pthread_create");
        }
        pthread_attr_destroy(&attr);
        
        ret=SERVER_RESPONSE_READY;
    }
    else{
        workerUnlock(worker);
        ret=SERVER_RESPONSE_BUSY;
    }
    return ret;
}

ServerResponse clientRequest_START(DataStream* ds_src,Worker* worker){
    int ret=0;
    if(workerLock(worker)) return 0;
    if(worker->workerState==WORKER_RESERVED){
        if(!compareAdresses(ds_src,&(worker->srcStream))){
            memcpy(&(worker->srcStream),ds_src,sizeof(DataStream));
            worker->thread=worker->srcStream.thread;
            workerUnlock(worker);
            launchWorker(worker);
            ret=SERVER_RESPONSE_ACCEPTED;
        }
        else{
            workerUnlock(worker);
            ret=SERVER_RESPONSE_BADCLIENT;
        }
    }
    else{
        workerUnlock(worker);
        ret=SERVER_RESPONSE_NOTRESERVED;
    }
    return ret;
}

ServerResponse clientRequest_STOP(DataStream* ds_src,Worker* worker){
    worker->cont=1;
    int ret=0;
    if(workerLock(worker)) return 0;
    if(worker->workerState==WORKER_BUSY){
        if(!compareAdresses(ds_src,&(worker->srcStream))){
            memcpy(&(worker->srcStream),ds_src,sizeof(DataStream));
            worker->workerState=WORKER_CANCEL;
            workerUnlock(worker);
            ret=SERVER_RESPONSE_ABORTED;
        }
        else{
            workerUnlock(worker);
            ret=SERVER_RESPONSE_BADCLIENT;
        }
    }
    else{
        workerUnlock(worker);
        ret=SERVER_RESPONSE_NOTRUNNING;
    }
    return ret;
    
}

char* unknownRequestStringCreator(const char* string,size_t size){
    const char* s1="Unknown Request ";
    size_t s1size=strlen(s1);
    char* tmp=(char*)malloc(s1size+size+3);
    snprintf(tmp,s1size+size+3,"%s'%.*s'",s1,(int)size,string);
    return tmp;
}

ServerResponse handleClientRequestDataStream(DataStream* ds_src,Worker* worker){
    if(!ds_src || !worker) return 0;
    char buffer[256];
    ssize_t size;
    if((size=parseNextStream(ds_src->fd,buffer,256))>0){
        ClientRequest cr=getClientRequestFromString_size(buffer,size);
        if(cr<CLIENT_REQUEST_UNKNOWN){
            connectionPrinter(stdout,ds_src->src_address.sin_addr,ds_src->src_address.sin_port,TCP_INCOMING_PREFIX,getClientRequestStringFromVal(cr));
            return ClientRequestFuncs[cr].clientRequestFunc(ds_src,worker);
        }
        else{
            char* tmp=unknownRequestStringCreator(buffer,size);
            connectionPrinter(stderr,ds_src->src_address.sin_addr,ds_src->src_address.sin_port,TCP_INCOMING_PREFIX,tmp);
            free(tmp);
        }
    }
    return 0;
}
