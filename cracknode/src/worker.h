#ifndef _WORKER_H
#define _WORKER_H

#include "connectionlistener.h"

#include <stddef.h>
#include <pthread.h>
#include <netinet/in.h>

typedef enum{
    WORKER_WAITING,
    WORKER_RESERVED,
    WORKER_BUSY,
    WORKER_CANCEL,
    WORKER_UNKNOWN
}WorkerState;

typedef enum{
    CRACKMETHOD_BRUTEFORCE,
    CRACKMETHOD_DICT,
    CRACKMETHOD_RAINBOW,
    CRACKMETHOD_UNKNOWN
}CrackMethod;

typedef struct{
    pthread_t thread;
    pthread_mutex_t workerMutex;
    WorkerState workerState;
    CrackMethod crackMethod;
    DataStream srcStream;
    char* cryptHash;
    char* password;
    char cont;
}Worker;

extern Worker* MainWorker;

extern const char* CrackMethodStrings[];

extern const char* WorkerStateStrings[];

int initWorker(Worker* work);

int destroyWorker(Worker* work,int destroyMutex);

WorkerState getWorkerStateFromString(const char* string);
WorkerState getWorkerStateFromString_size(const char* string,size_t size);
const char* getWorkerStateStringFromVal(WorkerState ws);

CrackMethod getCrackMethodFromString(const char* string);
CrackMethod getCrackMethodFromString_size(const char* string,size_t size);

const char* getCrackMethodStringFromVal(CrackMethod cm);

/*
 * Performs safe mutex lock
 */
WorkerState getWorkerState(Worker *worker);

int launchWorker(Worker* work);

/*
 * Performs safe mutex lock
 * Returned String if not NULL must be liberated
 */
char* getWorkerPassword(Worker* worker);

int workerLock(Worker* worker);

int workerUnlock(Worker* worker);

DataStream getWorkerDataStream(Worker* worker);

void freeWorker(Worker* work);
#endif
