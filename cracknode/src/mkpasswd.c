#define _GNU_SOURCE
#include <crypt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>

#define DEFAULT_HASH_TYPE "6"

#define FLAG_ALGO 1
#define FLAG_PASS 2
#define FLAG_SALT 4

#define OPTION_ALGO 'a'
#define OPTION_PASS 'p'
#define OPTION_SALT 's'
#define OPTION_HELP 'h'

#define PASS_MAX 4096
#define SALT_MAX_SIZE 8

#define RANDOM_FILE "/dev/urandom"
#define PASSWORD_PROMPT "Password: "

const char* HashVal[]={"1","5","6",NULL};
const char* HashStrings[]={
    "md5","sha256","sha512",NULL
};
const char* HashComments[]={
    "for MD5 hashing",
    "for SHA-256 hashing",
    "for SHA-512 hashing",
    NULL
};

const char* SaltChars="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXY0123456789./";

void printUsage(const char* appname){
    int i;
    printf("%s [-%c ",appname,OPTION_ALGO);
    for(i=0;HashVal[i] && HashStrings[i];++i){
        if(i) printf("|%s|%s",HashVal[i],HashStrings[i]);
        else printf("%s|%s",HashVal[i],HashStrings[i]);
    }
    printf("] [-%c password] [-%c salt]\n",OPTION_PASS,OPTION_SALT);
    printf("Usage:\n");
    printf("\t-%c: crypting algorithm.\n",OPTION_ALGO);
    for(i=0;HashVal[i] && HashStrings[i] && HashComments[i];++i){
        printf("\t\t%s or %s %s.%s\n",HashVal[i],HashStrings[i],HashComments[i],(!strcmp(HashVal[i],DEFAULT_HASH_TYPE))?"(Default)":"");
    }
    printf("\t-%c: supply password to hash on the command line. Otherwise it is asked to be typed in.\n",OPTION_PASS);
    printf("\t-%c: supply salt between 1 and %d characters. If none supplied it is generated.\n",OPTION_SALT,SALT_MAX_SIZE);
    printf("\t-%c: to print this help.\n",OPTION_HELP);
}

const char* getHashType(const char* val){
    if(val){
        int i;
        for(i=0;HashVal[i] && HashStrings[i];++i){
            if(!strcmp(val,HashVal[i]) || !strcmp(val,HashStrings[i])){
                return HashVal[i];
            }
        }
    }
    return NULL;
}

void clearBuffer(char* buffer,unsigned int maxsize){
    unsigned int i;
    for(i=0;i<maxsize;++i) buffer[i]=0;
}

unsigned int getPassword(const char* prompt,char* buffer,unsigned int maxsize){
    struct termios oldt,newt;
    int c;
    unsigned int i;
    fprintf(stderr,"%s",prompt);
    fflush(stderr);
    tcgetattr(STDIN_FILENO,&oldt);
    newt=oldt;
    newt.c_lflag&=~(ECHO);
    tcsetattr(STDIN_FILENO,TCSANOW,&newt);
    for(i=0;(c=getchar())!='\n' && c!=EOF && i<(maxsize-1);++i) buffer[i]=c;
    buffer[i]=0;
    tcsetattr(STDIN_FILENO,TCSANOW,&oldt);
    fprintf(stderr,"\n");
    return i;
}

char* generateSalt(char* saltbuffer,unsigned int maxsize){
    int randomfd;
    unsigned int randint;
    ssize_t res;
    size_t sca;
    unsigned int i;
    if((randomfd=open(RANDOM_FILE,O_RDONLY))<0) return NULL;
    maxsize=(maxsize<=SALT_MAX_SIZE)?maxsize-1:SALT_MAX_SIZE;
    sca=strlen(SaltChars);
    for(i=0;i<maxsize;++i){
        if((res=read(randomfd,&randint,sizeof(unsigned int)))<0) return NULL;
        saltbuffer[i]=SaltChars[(unsigned int)((((double)randint)/UINT_MAX)*sca)];
    }
    close(randomfd);
    saltbuffer[i]=0;
    return saltbuffer;
}

char checkSalt(const char* salt){
    int i;
    for(i=0;salt[i];++i){
        if(i>=SALT_MAX_SIZE) return 0;
        if(!(((salt[i]>='a') && (salt[i]<='z')) ||
            ((salt[i]>='A') && (salt[i]<='Z')) ||
            ((salt[i]>='0') && (salt[i]<='9')) ||
            (salt[i]=='.') || (salt[i]=='/'))) return 0;
    }
    return 1;
}

char* mycrypt(const char* password,const char* hashtype,const char* salt,struct crypt_data *data){
    char temp[1+4+1+SALT_MAX_SIZE+1];
    snprintf(temp,1+4+1+SALT_MAX_SIZE+1,"$%s$%s",hashtype,(salt)?salt:"");
    data->initialized=0;
    return crypt_r(password,temp,data);
}

int main(int argc, char* argv[]){
    char flags,*password,*salt,passwordbuffer[PASS_MAX],saltbuffer[SALT_MAX_SIZE+1];
    const char *hash;
    unsigned int size;
    struct crypt_data data;
    flags=0;
    password=NULL;
    hash=DEFAULT_HASH_TYPE;
    salt=NULL;
    if(argc>1){
        const char* appname;
        appname=*argv;
        for(++argv;*argv;++argv){
            if((**argv=='-') && (*((*argv)+1))){
                if(!(*((*argv)+2))){
                    switch((*((*argv)+1))){
                        case OPTION_ALGO:
                            if(*(++argv)){
                                if(flags & FLAG_ALGO){
                                    fprintf(stderr,"-%c was already specified\n",OPTION_ALGO);
                                    return EXIT_FAILURE;
                                }
                                else{
                                    flags|=FLAG_ALGO;
                                    if(!(hash=getHashType(*argv))){
                                        fprintf(stderr,"Unknown hash algorithm '%s'\n",*argv);
                                        printUsage(appname);
                                        return EXIT_FAILURE;
                                    }
                                }
                            }
                            else{
                                fprintf(stderr,"Expected Crypting Algorithm!\n");
                                printUsage(appname);
                                return EXIT_FAILURE;
                            }
                            break;
                        case OPTION_PASS:
                            if(*(++argv)){
                                if(flags & FLAG_PASS){
                                    fprintf(stderr,"-%c was already specified\n",OPTION_PASS);
                                    return EXIT_FAILURE;
                                }
                                else{
                                    flags|=FLAG_PASS;
                                    if(!(**argv)){
                                        fprintf(stderr,"Empty Password not accepted\n");
                                        return EXIT_FAILURE;
                                    }
                                    else{ password=*argv; size=strlen(password); }
                                }
                            }
                            else{
                                fprintf(stderr,"Expected password\n");
                                printUsage(appname);
                                return EXIT_FAILURE;
                            }
                            break;
                        case OPTION_SALT:
                            if(*(++argv)){
                                if(flags & FLAG_SALT){
                                    fprintf(stderr,"-%c was already specified\n",OPTION_SALT);
                                    return EXIT_FAILURE;
                                }
                                else{
                                    flags|=FLAG_SALT;
                                    if(!checkSalt(*argv)){
                                        fprintf(stderr,"Unacceptable salt '%s'\n",*argv);
                                        return EXIT_FAILURE;
                                    }
                                    else salt=*argv;
                                }
                            }
                            else{
                                fprintf(stderr,"Expected salt\n");
                                printUsage(appname);
                                return EXIT_FAILURE;
                            }
                            break;
                        case OPTION_HELP:
                            printUsage(appname);
                            return EXIT_SUCCESS;
                            break;
                        default:
                            fprintf(stderr,"Unknown option '%s'\n",*argv);
                            return EXIT_FAILURE;
                    }
                }
                else{
                    fprintf(stderr,"Unknown option '%s'\n",*argv);
                    return EXIT_FAILURE;
                }
            }
            else{
                fprintf(stderr,"Unknown option '%s'\n",*argv);
                return EXIT_FAILURE;
            }
        }
    }
    
    if(!(flags & FLAG_PASS)){
        for(;!(size=getPassword(PASSWORD_PROMPT,passwordbuffer,PASS_MAX));){
            fprintf(stderr,"Empty Password not accepted\n");
        }
        password=passwordbuffer;
    }
    
    if(!(flags & FLAG_SALT)){
        if(!generateSalt(saltbuffer,SALT_MAX_SIZE+1)){
            perror(RANDOM_FILE);
            return EXIT_FAILURE;
        }
        salt=saltbuffer;
    }
    
    printf("%s",mycrypt(password,hash,salt,&data));
    clearBuffer(password,size);
    return EXIT_SUCCESS;
}
