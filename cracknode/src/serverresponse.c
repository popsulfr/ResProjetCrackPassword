#include "serverresponse.h"
#include "connectionlistener.h"
#include "worker.h"
#include "datastreamsender.h"

#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

const char* ServerResponseStrings[]={
    "SIGNAL",
    "READY",
    "BUSY",
    "ACCEPTED",
    "ABORTED",
    "DONE",
    "NOTRUNNING",
    "BADCLIENT",
    "NOTRESERVED",
    NULL
};

struct _serverResponseWrapper{
    int fd;
    ServerResponse sr;
    Worker* worker;
};

int serverResponse_SIGNAL(struct _serverResponseWrapper*);
int serverResponse_READY(struct _serverResponseWrapper*);
int serverResponse_BUSY(struct _serverResponseWrapper*);
int serverResponse_ACCEPTED(struct _serverResponseWrapper*);
int serverResponse_ABORTED(struct _serverResponseWrapper*);
int serverResponse_DONE(struct _serverResponseWrapper*);
int serverResponse_NOTRUNNING(struct _serverResponseWrapper*);
int serverResponse_BADCLIENT(struct _serverResponseWrapper*);
int serverResponse_NOTRESERVED(struct _serverResponseWrapper*);


struct serverResponseThreadFunc{int (*serverResponseThreadFunc)(struct _serverResponseWrapper*);};

struct serverResponseThreadFunc ServerResponseThreadFuncs[]={
    {serverResponse_SIGNAL},
    {serverResponse_READY},
    {serverResponse_BUSY},
    {serverResponse_ACCEPTED},
    {serverResponse_ABORTED},
    {serverResponse_DONE},
    {serverResponse_NOTRUNNING},
    {serverResponse_BADCLIENT},
    {serverResponse_NOTRESERVED},
    {NULL}
};

ServerResponse getServerResponseFromString(const char* string){
    ServerResponse sr=SERVER_RESPONSE_UNKNOWN;
    if(string)
        for(sr=0;sr<SERVER_RESPONSE_UNKNOWN && ServerResponseStrings[sr] && strcmp(string,ServerResponseStrings[sr]);++sr);
    return sr;
}

ServerResponse getServerResponseFromString_size(const char* string,size_t size){
    ServerResponse sr=SERVER_RESPONSE_UNKNOWN;
    if(string)
        for(sr=0;sr<SERVER_RESPONSE_UNKNOWN && ServerResponseStrings[sr] && strncmp(string,ServerResponseStrings[sr],size);++sr);
    return sr;
}

const char* getServerResponseStringFromVal(ServerResponse sr){
    return (sr<=SERVER_RESPONSE_UNKNOWN)?ServerResponseStrings[sr]:NULL;
}

ssize_t writeOut(int fd,const void *data, size_t size){
    ssize_t ret;
    if((ret=write(fd,data,size))<0) perror("writeOut: write");
    return ret;
}

ssize_t writeOutSeparator(int fd){
    const char s=SERVER_SEPARATOR;
    return writeOut(fd,&s,1);
}

ssize_t writeOutHeader(int fd){
    char* h=SERVER_HEADER;
    return writeOut(fd,h,strlen(h));
}

void serverResponseWrapperCleaner(void* data){
    if(data){
        close(((struct _serverResponseWrapper*)data)->fd);
        free(data);
    }
}

int serverResponse_SIGNAL(struct _serverResponseWrapper* srw){
    const char* ws=getWorkerStateStringFromVal(getWorkerState(srw->worker));
    if(ws){
        if(writeOut(srw->fd,ws,strlen(ws))<0) return 0;
    }
    return 1;
}

int serverResponse_READY(struct _serverResponseWrapper* srw){
    (void)srw;
    return 1;
}

int serverResponse_BUSY(struct _serverResponseWrapper* srw){
    (void)srw;
    return 1;
}

int serverResponse_ACCEPTED(struct _serverResponseWrapper* srw){
    (void)srw;
    return 1;
}

int serverResponse_ABORTED(struct _serverResponseWrapper* srw){
    (void)srw;
    return 1;
}

int serverResponse_DONE(struct _serverResponseWrapper* srw){
    int ret=1;
    char* password=getWorkerPassword(srw->worker);
    if(password){
        if(writeOut(srw->fd,password,strlen(password))<0) ret=0;
        free(password);
    }
    return ret;
}

int serverResponse_NOTRUNNING(struct _serverResponseWrapper* srw){
    (void)srw;
    return 1;
}

int serverResponse_BADCLIENT(struct _serverResponseWrapper* srw){
    (void)srw;
    return 1;
}

int serverResponse_NOTRESERVED(struct _serverResponseWrapper* srw){
    (void)srw;
    return 1;
}

void* serverResponseThread(void* data){
    pthread_cleanup_push(serverResponseWrapperCleaner,data);
    struct _serverResponseWrapper *srw=(struct _serverResponseWrapper*)data;
    if(writeOutHeader(srw->fd)<0){pthread_exit(NULL);}
    if(writeOutSeparator(srw->fd)<0){ pthread_exit(NULL);}
    const char *t=getServerResponseStringFromVal(srw->sr);
    if(writeOut(srw->fd,t,strlen(t))<0){pthread_exit(NULL);}
    if(writeOutSeparator(srw->fd)<0){pthread_exit(NULL);}
    ServerResponseThreadFuncs[srw->sr].serverResponseThreadFunc(srw);
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}

int serverResponseReaderCreator(DataStream* ds,ServerResponse sr,Worker* worker){
    int p[2];
    if(pipe(p)<0){
        perror("serverResponseReaderCreator: pipe");
        return 0;
    }
    struct _serverResponseWrapper *srw=(struct _serverResponseWrapper*)malloc(sizeof(struct _serverResponseWrapper));
    srw->fd=p[1];
    srw->sr=sr;
    srw->worker=worker;
    
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr,PTHREAD_CREATE_DETACHED);
    
    if(pthread_create(&(ds->thread),&attr,serverResponseThread,srw)){
        perror("serverResponseReaderCreator: pthread_create");
        pthread_attr_destroy(&attr);
        close(p[0]);
        close(p[1]);
        free(srw);
        return 0;
    }
    else ds->fd=p[0];
    pthread_attr_destroy(&attr);
    return 1;
}

int serverResponseDataStreamCreator(DataStream *ds,const struct sockaddr_in *srcAddr,ServerResponse sr,Worker *worker){
    if(!ds || !srcAddr) return 0;
    memcpy(&(ds->src_address),srcAddr,sizeof(struct sockaddr_in));
    return serverResponseReaderCreator(ds,sr,worker);
}

int responseStreamSender(Worker* worker,ServerResponse sr){
    DataStream dsdst;
    if(!serverResponseDataStreamCreator(&dsdst,&(worker->srcStream.src_address),sr,worker)){
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.listenPort,TCP_OUTGOING_PREFIX,"Unable to create Server response");
        workerUnlock(worker);
        return 0;
    }
    
    struct sockaddr_in dest;
    workerLock(worker);
    memcpy(&dest,&(worker->srcStream.src_address),sizeof(struct sockaddr_in));
    dest.sin_port=worker->srcStream.listenPort;
    workerUnlock(worker);
    int fd;
    workerLock(worker);
    if((fd=worker->srcStream.fd=createConnectSocket(&(dest)))<0){
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.listenPort,TCP_OUTGOING_PREFIX,"Unable to connect");
        workerUnlock(worker);
        socketReaderCloser(&dsdst);
        return 0;
    }
    workerUnlock(worker);
    
    workerLock(worker);
    connectionPrinter(stdout,worker->srcStream.src_address.sin_addr,worker->srcStream.listenPort,TCP_OUTGOING_PREFIX,getServerResponseStringFromVal(sr));
    workerUnlock(worker);
    if(transferStream(fd,&dsdst)){
        workerLock(worker);
        connectionPrinter(stdout,worker->srcStream.src_address.sin_addr,worker->srcStream.listenPort,TCP_OUTGOING_PREFIX,"Response Transfer success");
    }
    else{
        workerLock(worker);
        connectionPrinter(stderr,worker->srcStream.src_address.sin_addr,worker->srcStream.listenPort,TCP_OUTGOING_PREFIX,"Response Transfer failed");
    }
    workerUnlock(worker);
    
    workerLock(worker);
    socketWriterCloser(&(worker->srcStream.fd));
    workerUnlock(worker);
    
    socketReaderCloser(&dsdst);
    return 1;
}
