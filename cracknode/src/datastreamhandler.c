#include "datastreamhandler.h"
#include "connectionlistener.h"
#include "clientrequest.h"
#include "worker.h"
#include "datastreamsender.h"
#include "serverresponse.h"

#include <string.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

void streamHandler(DataStream *ds){
    ServerResponse sr;
    pthread_cleanup_push(socketWriterCloser,&(ds->fd));
    connectionPrinter(stdout,ds->src_address.sin_addr,ds->src_address.sin_port,TCP_INCOMING_PREFIX,NULL);
    if((sr=handleClientRequestDataStream(ds,MainWorker))==SERVER_RESPONSE_UNKNOWN){socketWriterCloser(&(ds->fd)); return;}
    
    DataStream dsdst;
    if(!serverResponseDataStreamCreator(&dsdst,&(ds->src_address),sr,MainWorker)){
        connectionPrinter(stderr,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,"Unable to create Server response");
        socketWriterCloser(&(ds->fd));
        return;
    }
    pthread_cleanup_push(socketReaderCloser,&dsdst);
    connectionPrinter(stdout,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,getServerResponseStringFromVal(sr));
    if(transferStream(ds->fd,&dsdst)){
        connectionPrinter(stdout,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,"Response Transfer success");
    }
    else{
        connectionPrinter(stderr,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,"Connection closed by client. Establishing new connection.");
        socketReaderCloser(&dsdst);
        socketWriterCloser(&(ds->fd));
        struct sockaddr_in dest;
        memcpy(&dest,&(ds->src_address),sizeof(struct sockaddr_in));
        dest.sin_port=ds->listenPort;
        int fd;
        if((fd=createConnectSocket(&(dest)))<0){
            connectionPrinter(stderr,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,"Unable to connect");
            socketReaderCloser(&dsdst);
            socketWriterCloser(&(fd));
            socketWriterCloser(&(ds->fd));
            return;
        }
        if(!serverResponseDataStreamCreator(&dsdst,&(ds->src_address),sr,MainWorker)){
            connectionPrinter(stderr,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,"Unable to create Server response");
            socketReaderCloser(&dsdst);
            socketWriterCloser(&(fd));
            socketWriterCloser(&(ds->fd));
            return;
        }
        if(transferStream(fd,&dsdst)){
            connectionPrinter(stdout,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,"Response Transfer success");
        }
        else{
            connectionPrinter(stderr,ds->src_address.sin_addr,ds->listenPort,TCP_OUTGOING_PREFIX,"Response Transfer failed");
        }
        socketWriterCloser(&(ds->fd));
    }
    pthread_cleanup_pop(1);
    while(!(MainWorker->cont));
    MainWorker->cont=0;
    pthread_cleanup_pop(1);
}
