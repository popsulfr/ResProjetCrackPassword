#ifndef _DATASTREAMSENDER_H
#define _DATASTREAMSENDER_H

#include "connectionlistener.h"

int createConnectSocket(struct sockaddr_in* dest);

/*
 * Connects using the socket to the sockaddr_in specified in the DataStream
 * Reads from the passed file descriptor 
 */
int transferStream(int socket,DataStream *ds);

void socketReaderCloser(void* data);

void socketWriterCloser(void* data);
#endif
