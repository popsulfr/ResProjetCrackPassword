#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <pthread.h>

#include "connectionlistener.h"
#include "datapackethandler.h"
#include "datastreamhandler.h"
#include "worker.h"

#define DEFAULT_PORT 1664

int usr_interrupt=0;

Worker _worker;

void cleanUp(){
    if(UDPList) destroyListener(UDPList);
    if(TCPList) destroyListener(TCPList);
    if(MainWorker) destroyWorker(MainWorker,1);
}

void handleSignal(int signal){
    (void)signal;
    usr_interrupt=1;
}

int main(){
    atexit(cleanUp);
    struct sigaction action;
    action.sa_handler=SIG_IGN;
    sigemptyset(&(action.sa_mask));
    action.sa_flags=0;
    
    if(sigaction(SIGPIPE,&action,NULL)<0){
        perror("main: sigaction");
        exit(EXIT_FAILURE);
    }
    action.sa_handler=handleSignal;
    if(sigaction(SIGINT,&action,NULL)<0){
        perror("main: sigaction");
        exit(EXIT_FAILURE);
    }
    if(sigaction(SIGTERM,&action,NULL)<0){
        perror("main: sigaction");
        exit(EXIT_FAILURE);
    }
    
    sigset_t mask,oldmask;
    sigemptyset(&mask);
    sigaddset(&mask,SIGINT);
    sigaddset(&mask,SIGTERM);
    fprintf(stderr,"Pid: %d\n",getpid());
    fprintf(stderr,"Waking up...\n");
    if(!initWorker(&_worker)) exit(EXIT_SUCCESS);
    MainWorker=&_worker;
    if(!listenConnectionsUDP(DEFAULT_PORT,&_udpList,packetHandler)){
        exit(EXIT_FAILURE);
    }
    UDPList=&_udpList;
    if(!listenConnectionsTCP(DEFAULT_PORT,&_tcpList,streamHandler)){
        exit(EXIT_FAILURE);
    }
    TCPList=&_tcpList;
    sigprocmask(SIG_BLOCK,&mask,&oldmask);
    while(!usr_interrupt) sigsuspend(&oldmask);
    sigprocmask(SIG_UNBLOCK,&mask,NULL);
    fprintf(stderr,"\nGoing to sleep...\n");
    exit(EXIT_SUCCESS);
}
