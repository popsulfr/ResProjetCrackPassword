#ifndef _SERVERRESPONSE_H
#define _SERVERRESPONSE_H

#include "connectionlistener.h"
#include "worker.h"

#include <stddef.h>
#include <netinet/in.h>

#define SERVER_HEADER "CRACKNODE"
#define SERVER_SEPARATOR '%'

typedef enum{
    SERVER_RESPONSE_SIGNAL,
    SERVER_RESPONSE_READY,
    SERVER_RESPONSE_BUSY,
    SERVER_RESPONSE_ACCEPTED,
    SERVER_RESPONSE_ABORTED,
    SERVER_RESPONSE_DONE,
    SERVER_RESPONSE_NOTRUNNING,
    SERVER_RESPONSE_BADCLIENT,
    SERVER_RESPONSE_NOTRESERVED,
    SERVER_RESPONSE_UNKNOWN
}ServerResponse;

extern const char* ServerResponseStrings[];

ServerResponse getServerResponseFromString(const char* string);
ServerResponse getServerResponseFromString_size(const char* string,size_t size);

const char* getServerResponseStringFromVal(ServerResponse sr);


int serverResponseDataStreamCreator(DataStream *ds,const struct sockaddr_in *srcAddr,ServerResponse sr,Worker* worker);

int responseStreamSender(Worker* worker,ServerResponse sr);
#endif
