#include "rainbow.h"
#include "commoncrypt.h"
#include "worker.h"
#include "serverresponse.h"

#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define BUFFER_SIZE 2048
#define DELIMITER "\n"

void cleaner(void* data){
    if(data && *((char**)data)) free(*((char**)data));
}

int rainbowCheck(Worker* worker){
    workerLock(worker);
    int fd=worker->srcStream.fd;
    workerUnlock(worker);
    
    char buffer[BUFFER_SIZE];
    ssize_t size;
    
    size_t off=0;
    char* lastToken=NULL;
    pthread_cleanup_push(cleaner,&lastToken);
    
    for(;(size=read(fd,buffer+off,BUFFER_SIZE-1-off))>0 || (size==0 && off>0);){
        workerLock(worker);
        if(worker->workerState==WORKER_CANCEL){
            workerUnlock(worker);
            break;
        }
        workerUnlock(worker);
        buffer[size+off]=0;
        off=0;
        char* saveptr=NULL;
        char* s;
        unsigned long int i=0;
        for(s=strtok_r(buffer,DELIMITER,&saveptr);s;++i,s=strtok_r(NULL,DELIMITER,&saveptr)){
            if((buffer!=s) && (!saveptr || ((saveptr)-buffer)>=(BUFFER_SIZE-1))){
                memmove(buffer,s,(off=(BUFFER_SIZE-1)-(s-buffer)));
                break;
            }
            else{
                if((i%2)==1){
                    if(!strcmp(lastToken,worker->cryptHash)){
                        free(lastToken);
                        lastToken=NULL;
                        workerLock(worker);
                        worker->password=strcpy((char*)malloc(strlen(s)+1),s);
                        workerUnlock(worker);
                        return 1;
                    }
                    else{
                        free(lastToken);
                        lastToken=NULL;
                    }
                }
                else{
                    lastToken=strcpy((char*)malloc(strlen(s)+1),s);
                }
            }
        }
    }
    pthread_cleanup_pop(1);
    return 0;
}

void workerRainbowCleanUp(void *data){
    if(data){
        workerLock((Worker*)data);
        freeWorker((Worker*)data);
        workerUnlock((Worker*)data);
    }
}

void* rainbowThread(void* data){
    Worker *worker=(Worker*)data;
    pthread_cleanup_push(workerRainbowCleanUp,worker);
    rainbowCheck(worker);
    worker->cont=1;
    
    responseStreamSender(worker,SERVER_RESPONSE_DONE);
    pthread_cleanup_pop(1);
    pthread_exit(NULL);
    return NULL;
}


int rainbowStreamHandler(Worker* worker){
    workerLock(worker);
    if(pthread_create(&(worker->thread),NULL,rainbowThread,worker)){
        workerUnlock(worker);
        perror("rainbowStreamHandler: pthread_create");
        return 0;
    }
    workerUnlock(worker);
    return 1;
}
