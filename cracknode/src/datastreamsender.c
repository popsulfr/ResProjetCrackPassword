#include "datastreamsender.h"
#include "connectionlistener.h"

#include <sys/socket.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define BUFFER_SIZE 2048

int createConnectSocket(struct sockaddr_in* dest){
    if(!dest) return -1;
    int sock;
    if((sock=socketCreator(SOCK_STREAM,0))<0) return -1;
    
    struct sockaddr_in host_address;
    sockaddrFiller(&host_address,INADDR_ANY,0);
    
    if(socketBinder(sock,&host_address)<0) return -1;
    
    if(connect(sock,(struct sockaddr*)(dest),sizeof(struct sockaddr_in))<0){
        perror("transferStream: connect");
        return -1;
    }
    return sock;
}

/*
 * Connects usingthe socket to the sockaddr_in specified in the DataStream
 * Reads from the passed file descriptor 
 */
int transferStream(int socket,DataStream *ds){
    if(!ds || socket<0) return 0;
    ssize_t size;
    unsigned char* buffer[BUFFER_SIZE];
    for(;(size=read(ds->fd,buffer,BUFFER_SIZE))>0;){
        if(write(socket,buffer,size)<0){
            perror("transferStream: write");
            return 0;
        }
    }
    return 1;
}

void socketReaderCloser(void* data){
    if(data && (((DataStream*)data)->fd>=0)){
        pthread_cancel(((DataStream*)data)->thread);
        ((DataStream*)data)->thread=0;
        close(((DataStream*)data)->fd);
        ((DataStream*)data)->fd=-1;
    }
}

void socketWriterCloser(void* data){
    if(data && (*((int*)data)>=0)){
        closeConnection((int*)data);
    }
}
