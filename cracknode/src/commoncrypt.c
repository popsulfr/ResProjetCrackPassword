#define _GNU_SOURCE
#include <crypt.h>

#include "commoncrypt.h"
#include <stdio.h>
#include <string.h>


int checkPasswordWithHash(const char* password,const char* hash){
    struct crypt_data data;
    //fprintf(stdout,"Test: '%s', '%s'\n",password,hash);
    data.initialized=0;
    int val=!strcmp(hash,crypt_r(password,hash,&data));
    return val;
}
