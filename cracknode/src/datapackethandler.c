#include "datapackethandler.h"
#include "connectionlistener.h"
#include "datastreamsender.h"
#include "clientrequest.h"
#include "serverresponse.h"
#include "worker.h"

#include <string.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

void packetHandler(DataPacket *dp){
    ServerResponse sr;
    connectionPrinter(stdout,dp->src_address.sin_addr,dp->src_address.sin_port,UDP_INCOMING_PREFIX,NULL);
    if((sr=handleClientRequestDataPacket(dp,MainWorker))==SERVER_RESPONSE_UNKNOWN) return;
    
    DataStream ds;
    if(!serverResponseDataStreamCreator(&ds,&(dp->src_address),sr,MainWorker)) connectionPrinter(stderr,dp->src_address.sin_addr,dp->listenPort,TCP_OUTGOING_PREFIX,"Unable to create Server response");
    
    pthread_cleanup_push(socketReaderCloser,&ds);
    struct sockaddr_in dest;
    memcpy(&dest,&(dp->src_address),sizeof(struct sockaddr_in));
    dest.sin_port=dp->listenPort;
    int fd;
    if((fd=createConnectSocket(&(dest)))<0){
        connectionPrinter(stderr,dp->src_address.sin_addr,dp->listenPort,TCP_OUTGOING_PREFIX,"Unable to connect");
        return;
    }
    pthread_cleanup_push(socketWriterCloser,&(fd));
    connectionPrinter(stdout,dp->src_address.sin_addr,dp->listenPort,TCP_OUTGOING_PREFIX,getServerResponseStringFromVal(sr));
    if(transferStream(fd,&ds)){
        connectionPrinter(stdout,dp->src_address.sin_addr,dp->listenPort,TCP_OUTGOING_PREFIX,"Response Transfer success");
    }
    else{
        connectionPrinter(stderr,dp->src_address.sin_addr,dp->listenPort,TCP_OUTGOING_PREFIX,"Response Transfer failed");
    }
    pthread_cleanup_pop(1);
    pthread_cleanup_pop(1);
}
