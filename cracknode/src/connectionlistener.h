#ifndef _CONNECTIONLISTENER_H
#define _CONNECTIONLISTENER_H

#include <stdint.h>
#include <pthread.h>
#include <netinet/in.h>
#include <stdio.h>

#define MAX_CONNECTIONS 20

#define TCP_INCOMING_PREFIX "TCP Incoming"
#define TCP_OUTGOING_PREFIX "TCP Outgoing"
#define UDP_INCOMING_PREFIX "UDP Incoming"
#define UDP_OUTGOING_PREFIX "UDP Outgoing"

typedef struct{
    int sock;
    pthread_t thread;
}Listener;

typedef struct{
    struct sockaddr_in src_address;
    in_port_t listenPort;
    size_t length;
    char* data;
    pthread_t thread;
}DataPacket;

typedef struct{
    struct sockaddr_in src_address;
    in_port_t listenPort;
    int fd;
    pthread_t thread;
}DataStream;

#define CONNECTION_ID_BUFFER_SIZE 1+INET_ADDRSTRLEN+1+5+1+1

typedef struct{
    char buffer[CONNECTION_ID_BUFFER_SIZE];
}ConnectionIdBuffer;

extern Listener _tcpList;
extern Listener *TCPList;

extern Listener _udpList;
extern Listener *UDPList;

void initDataPacket(DataPacket *dp);
void initDataStream(DataStream *ds);

int socketCreator(int type, int protocol);
void sockaddrFiller(struct sockaddr_in* address,uint32_t hostaddr,uint16_t port);
int socketBinder(int sock,struct sockaddr_in* address);

/*
 * Destroys the open socket and joins the listener Thread
 */
void destroyListener(Listener *list);

/*
* Creates UDP server on port, the Listener structure is filled with socket and thread
* packetHandler handles the incoming packets
* The recvfrom loop is in its own thread
*/
int listenConnectionsUDP(uint16_t port,Listener *list,void (*packetHandler)(DataPacket*));

/*
 * Creates a TCP Server on port, the Listener structure is filled with the socket and thread
 * A streamHandler is used to handle the incoming stream
 * The listener loop is in its own thread
 */
int listenConnectionsTCP(uint16_t port,Listener *list,void (*streamHandler)(DataStream *));

ssize_t parseNextPacket(const char* buffer,size_t offset,size_t size);
ssize_t parseNextStream(int fd,char *buffer,size_t size);
ssize_t skipStream(int fd,size_t size);

/*
 * Prints '[ip:port] '
 */
void connectionPrinter(FILE *file,struct in_addr addr,in_port_t port,const char* prefix,const char* addInfo);

char* connectionIdCreator(ConnectionIdBuffer* cid,struct in_addr addr,in_port_t port);

int compareAdresses(const DataStream* ds1,const DataStream* ds2);

void closeConnection(int* fd);
#endif
