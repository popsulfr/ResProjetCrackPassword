#include <stdio.h>
#include <string.h>
//#include <math.h>
#include <stdlib.h>

unsigned long long int mypower(unsigned int a,unsigned int b){
    unsigned long long int c=1;
    for(;b>0;--b) c*=a;
    return c;
}

void bruteforce(const char* alphabet,unsigned int maxwordsize,unsigned long long int begel){
    unsigned int alphsize;
    unsigned long long int maxcombinations;
    unsigned int i;
    unsigned int begelws;
    unsigned long long int begelwsnumber;
    alphsize=strlen(alphabet);
    maxcombinations=0;
    for(i=1,begelws=1,begelwsnumber=0;i<=maxwordsize;++i){
        unsigned long long int power=mypower(alphsize,i);
        //printf("power: %u\n",power);
        if(begel>=maxcombinations && begel<(maxcombinations+power)){
            begelws=i;
            begelwsnumber=begel-maxcombinations;
        }
        maxcombinations+=power;
    }
    //printf("begelwsnumber: %u, begelws: %u\n",begelwsnumber,begelws);
    printf("Maxcombinations: %llu\n",maxcombinations);
    if(begel>=maxcombinations){ fprintf(stderr,"begel out of range\n"); return;}
    printf("Searched word %lluth element of word size %u\n",begelwsnumber,begelws);
    
    char* word=(char*)malloc(sizeof(char)*(begelws+1));
    unsigned long long int begelwsnmod=begelwsnumber+1;
    for(i=0;i<begelws;++i){
        unsigned long long int newpow=mypower(alphsize,begelws-(i+1));
        //printf("begelwsnmod: %u\n",begelwsnmod);
        //printf("truc number: %u\n",(((begelwsnumber%begelwsnmod))/newpow));
        word[i]=alphabet[(((begelwsnumber%begelwsnmod))/newpow)];
        begelwsnmod=newpow;
    }
    word[i]=0;
    printf("Word: '%s'\n",word);
    free(word);
}

int main(){
    //bruteforce("abcdefghijklmnopqrstuvwxyz",64,3752126);
    bruteforce("0123456789abcdefghijklmnopqrstuvwxyz",16,(unsigned long long int)13596759648027234419);
    return 1;
}
