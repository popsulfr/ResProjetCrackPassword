#ifndef _CLIENTREQUEST_H
#define _CLIENTREQUEST_H

#include "worker.h"
#include "connectionlistener.h"
#include "serverresponse.h"

#include <stddef.h>

#define CLIENT_HEADER "CRACKNODECLIENT"
#define CLIENT_SEPARATOR '%'

#define CLIENT_PREPARE_TIMEOUT 10

typedef enum{
    CLIENT_REQUEST_DISCOVER,
    CLIENT_REQUEST_PREPARE,
    CLIENT_REQUEST_START,
    CLIENT_REQUEST_STOP,
    CLIENT_REQUEST_UNKNOWN
}ClientRequest;

extern const char* ClientRequestStrings[];

ClientRequest getClientRequestFromString(const char* string);
ClientRequest getClientRequestFromString_size(const char* string,size_t size);

const char* getClientRequestStringFromVal(ClientRequest cr);

ServerResponse handleClientRequestDataPacket(const DataPacket* dp_src,Worker* worker);
ServerResponse handleClientRequestDataStream(DataStream* ds_src,Worker* worker);
#endif
