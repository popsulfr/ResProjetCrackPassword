#include "worker.h"
#include "connectionlistener.h"
#include "bruteforce.h"
#include "serverresponse.h"
#include "datastreamsender.h"
#include "dict.h"
#include "rainbow.h"

#include <string.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFFER_SIZE 2048

Worker* MainWorker=NULL;

const char* WorkerStateStrings[]={
    "WAITING",
    "RESERVED",
    "BUSY",
    "CANCEL",
    NULL
};

const char* CrackMethodStrings[]={
    "BRUTEFORCE",
    "DICT",
    "RAINBOW",
    NULL
};

struct crackMethodHandlerFunc {int (*crackMethodHandlerFunc)(Worker*);};

struct crackMethodHandlerFunc CrackMethodHandlerFuncFuncs[]={
    {bruteforceStreamHandler},
    {dictStreamHandler},
    {rainbowStreamHandler},
    {NULL}
};

WorkerState getWorkerStateFromString(const char* string){
    WorkerState ws=WORKER_UNKNOWN;
    if(string)
        for(ws=0;ws<WORKER_UNKNOWN && strcmp(string,WorkerStateStrings[ws]);++ws);
    return ws;
}

WorkerState getWorkerStateFromString_size(const char* string,size_t size){
    WorkerState ws=WORKER_UNKNOWN;
    if(string)
        for(ws=0;ws<WORKER_UNKNOWN && strncmp(string,WorkerStateStrings[ws],size);++ws);
    return ws;
}

const char* getWorkerStateStringFromVal(WorkerState ws){
    return (ws<WORKER_UNKNOWN)?WorkerStateStrings[ws]:NULL;
}

CrackMethod getCrackMethodFromString(const char* string){
    CrackMethod crm=CRACKMETHOD_UNKNOWN;
    if(string)
        for(crm=0;crm<CRACKMETHOD_UNKNOWN && CrackMethodStrings[crm] && strcmp(string,CrackMethodStrings[crm]);++crm);
        return crm;
}

CrackMethod getCrackMethodFromString_size(const char* string,size_t size){
    CrackMethod crm=CRACKMETHOD_UNKNOWN;
    if(string)
        for(crm=0;crm<CRACKMETHOD_UNKNOWN && CrackMethodStrings[crm] && strncmp(string,CrackMethodStrings[crm],size);++crm);
        return crm;
}

const char* getCrackMethodStringFromVal(CrackMethod crm){
    return (crm<=CRACKMETHOD_UNKNOWN)?CrackMethodStrings[crm]:NULL;
}

int initWorker(Worker* work){
    if(work){
        if(pthread_mutex_init(&(work->workerMutex),NULL)){
            perror("initWorker: pthread_mutex_init");
            return 0;
        }
        if(workerLock(work)){
            perror("initWorker: workerLock");
            return 0;
        }
        work->cont=0;
        work->thread=0;
        work->workerState=WORKER_WAITING;
        work->crackMethod=CRACKMETHOD_UNKNOWN;
        work->password=NULL;
        work->cryptHash=NULL;
        initDataStream(&(work->srcStream));
        if(workerUnlock(work)){
            perror("initWorker: workerUnlock");
            return 0;
        }
        return 1;
    }
    return 0;
}

void freeWorker(Worker* work){
    if(work){
        work->cont=0;
        work->thread=0;
        work->workerState=WORKER_WAITING;
        work->crackMethod=CRACKMETHOD_UNKNOWN;
        if(work->password){
            free(work->password);
            work->password=NULL;
        }
        if(work->cryptHash){
            free(work->cryptHash);
            work->cryptHash=NULL;
        }
    }
    
}

int destroyWorker(Worker *work,int destroyMutex){
    if(work){
        workerLock(work);
        if(work->thread>0){
            work->workerState=WORKER_CANCEL;
            workerUnlock(work);
            /*
            if(pthread_cancel(work->thread)){
                perror("destroyWorker: pthread_cancel");
                workerUnlock(work);
                return 0;
            }
            */
            if(pthread_join(work->thread,NULL)){
                perror("destroyWorker: pthread_join");
            }
            workerLock(work);
            work->thread=0;
        }
        freeWorker(work);
        initDataStream(&(work->srcStream));
        workerUnlock(work);
        if(destroyMutex){
            if(pthread_mutex_destroy(&(work->workerMutex))){
                perror("destroyWorker: pthread_mutex_destroy");
                return 0;
            }
        }
        return 1;
    }
    return 0;
}


void workerParser(Worker *worker){
    char buffer[BUFFER_SIZE];
    ssize_t off;
    int fd;
    if(workerLock(worker)) return;
    fd=worker->srcStream.fd;
    if(workerUnlock(worker)) return;
    
    if((off=parseNextStream(fd,buffer,BUFFER_SIZE))<=0) return;
    
    if(workerLock(worker)) return;
    worker->cryptHash=strncpy((char*)malloc(off+1),buffer,off);
    (worker->cryptHash)[off]=0;
    if(workerUnlock(worker)) return;
    
    if((off=parseNextStream(fd,buffer,BUFFER_SIZE))<=0){
        if(workerLock(worker)) return;
        free(worker->cryptHash);
        worker->cryptHash=NULL;
        workerUnlock(worker);
        return;        
    }
    
    CrackMethod cm=getCrackMethodFromString_size(buffer,off);
    if(cm==CRACKMETHOD_UNKNOWN){
        fprintf(stderr,"Unknown CrackMethod: %.*s\n",(int)off,buffer);
        if(workerLock(worker)) return;
        free(worker->cryptHash);
        worker->cryptHash=NULL;
        workerUnlock(worker);
        return;
    }
    
    if(CrackMethodHandlerFuncFuncs[cm].crackMethodHandlerFunc){
        CrackMethodHandlerFuncFuncs[cm].crackMethodHandlerFunc(worker);
    }
}

int launchWorker(Worker* work){
    if(work){
        if(workerLock(work)) return 0;
        if(work->workerState!=WORKER_RESERVED){
            connectionPrinter(stderr,work->srcStream.src_address.sin_addr,work->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"The Worker was not prepared");
            workerUnlock(work);
            return 0;
        }
        if(work->workerState==WORKER_BUSY){
            connectionPrinter(stderr,work->srcStream.src_address.sin_addr,work->srcStream.src_address.sin_port,TCP_INCOMING_PREFIX,"The Worker is still running");
            workerUnlock(work);
            return 0;
        }
        work->workerState=WORKER_BUSY;
        workerUnlock(work);
        workerParser(work);
    }
    return 1;
}

WorkerState getWorkerState(Worker* worker){
    WorkerState ws=WORKER_UNKNOWN;
    if(worker){
        if(workerLock(worker)) return ws;
        ws=worker->workerState;
        workerUnlock(worker);
    }
    return ws;
}

char* getWorkerPassword(Worker* worker){
    char* password=NULL;
    if(worker){
        if(workerLock(worker)) return NULL;
        if(worker->password){
            password=(char*)malloc(strlen(worker->password)+1);
            strcpy(password,worker->password);
        }
        workerUnlock(worker);
    }
    return password;
}

int workerLock(Worker* worker){
    if(worker){
        if(pthread_mutex_lock(&(worker->workerMutex))){
            perror("workerLock: pthread_mutex_lock");
            return 1;
        }
    }
    return 0;
}

int workerUnlock(Worker* worker){
    if(worker){
        if(pthread_mutex_unlock(&(worker->workerMutex))){
            perror("workerUnlock: pthread_mutex_unlock");
            return 1;
        }
    }
    return 0;
}

DataStream getWorkerDataStream(Worker* worker){
    DataStream ds;
    memset(&ds,0,sizeof(DataStream));
    if(worker){
        if(workerLock(worker)) return ds;
        memcpy(&ds,&(worker->srcStream),sizeof(DataStream));
        workerUnlock(worker);
    }
    return ds;
}
